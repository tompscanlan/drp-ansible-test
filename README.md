# drp-ansible-test

The contents directory should be deployed to a Digital Rebar Platform.  Attaching the included profile to a machine will install paperless-ngx with a default config on the machine when running the attached workflow.

That workflow will check out the included ansible playbooks and run them on the machine, affecting a deploy of paperless-ngx.
